# frozen_string_literal: true

require 'arch_ry/booting/boot_loader'
require 'arch_ry/booting/initramfs'

module ArchRy
  # Booting is the process of starting a computer as initiated via hardware such as a button or by a software command.
  # After it is switched on, a computer's central processing unit (CPU) has no software in its main memory, so some
  # process must load software into memory before it can be executed.
  #
  # See https://en.wikipedia.org/wiki/Booting
  module Booting
    def self.efi_enabled?
      TTY::Command.quiet.run!(:ls, '/sys/firmware/efi/efivars').success?
    end
  end
end
