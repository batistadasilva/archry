# frozen_string_literal: true

module ArchRy
  module AccessControl
    # A password is secret data used to confirm a user's identity.
    #
    # See https://en.wikipedia.org/wiki/Password
    class Password
      attr_reader :user

      def initialize(user:)
        @user = user
      end

      def update!(new_value)
        TTY::Command.default.run :passwd, user.username, in: input_stream(new_value)
      end

      private

      def input_stream(value)
        in_stream = StringIO.new
        in_stream.puts value
        in_stream.puts value
        in_stream.rewind
        in_stream
      end
    end
  end
end
