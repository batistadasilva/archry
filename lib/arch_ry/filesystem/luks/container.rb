# frozen_string_literal: true

module ArchRy
  module Filesystem
    module LUKS
      # See https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#2-setup
      class Container < BlockDevice
        attr_reader :device, :type, :key_size, :hash

        def initialize(name:, device:, type: 'luks1', key_size: 512, hash: 'sha512')
          super()
          @name = name
          @device = device
          @type = type
          @key_size = key_size
          @hash = hash
        end

        def create!(passphrase)
          TTY::Command.default.run(:cryptsetup, 'luksFormat', '--type', type, '--use-random',
                                   '-s', key_size, '-h', hash, device.path, in: passphrase_stream(passphrase))
        end

        def generate_encryption_key!(passphrase)
          in_stream = StringIO.new
          in_stream.puts passphrase
          in_stream.rewind
          filepath = "/root/#{name}.keyfile"
          TTY::Command.default.run :dd, 'bs=512', 'count=4', 'if=/dev/random', "of=#{filepath}", 'iflag=fullblock'
          TTY::Command.default.run :chmod, '000', filepath
          TTY::Command.default.run :cryptsetup, '--verbose', 'luksAddKey', '-i', 1, device.path, filepath, in: in_stream
          filepath
        end

        def open(passphrase)
          TTY::Command.default.run :cryptsetup, 'open', device.path, name, in: passphrase_stream(passphrase)
        end

        def path
          "/dev/mapper/#{name}"
        end

        private

        def passphrase_stream(passphrase)
          in_stream = StringIO.new
          in_stream.puts passphrase
          in_stream.puts passphrase
          in_stream.rewind
          in_stream
        end
      end
    end
  end
end
