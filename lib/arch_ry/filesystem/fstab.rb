# frozen_string_literal: true

module ArchRy
  module Filesystem
    # The fstab file can be used to define how disk partitions, various other block devices, or remote file systems
    # should be mounted into the file system.
    #
    # See https://wiki.archlinux.org/title/Fstab
    class Fstab
      FILEPATH = '/etc/fstab'

      attr_reader :root_path

      def initialize(root_path: '/')
        @root_path = root_path
      end

      def create!(tag: 'UUID')
        TTY::Command.default.run(:genfstab, '-t', tag, root_path, out: "#{root_path}#{FILEPATH}")
      end

      def add_tmpfs!(size)
        TTY::Command.default.run(
          :echo, "tmpfs\t\t\t\t\t\t/tmp\t\ttmpfs\t\trw,nodev,nosuid,size=#{size}\t\t0 0",
          out: ["#{root_path}#{FILEPATH}", 'a']
        )
      end
    end
  end
end
