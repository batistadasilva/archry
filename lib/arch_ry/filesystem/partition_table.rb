# frozen_string_literal: true

module ArchRy
  module Filesystem
    # A partition table is a table maintained on a disk by the operating system that outlines and describes the
    # partitions on that disk.
    #
    # See https://en.wikipedia.org/wiki/Disk_partitioning#Partition_table
    class PartitionTable
      attr_reader :disk, :type

      def initialize(disk:, type:)
        @disk = disk
        @type = type
      end

      def create!
        TTY::Command.default.run <<-CMD
          fdisk #{disk.path} <<EOF
            #{partition_table_type_code(type)}
            w
          EOF
        CMD
      end

      def add_partition!(type, size: nil)
        TTY::Command.default.run <<-CMD
          fdisk #{disk.path} <<EOF
            n


            #{partition_size_code(size)}
            t

            #{partition_type_code(type)}
            w
          EOF
        CMD
        disk.partitions.last
      end

      private

      def partition_table_type_code(type)
        case type
        when :gpt # See https://en.wikipedia.org/wiki/GUID_Partition_Table
          'g'
        when :mbr # See https://en.wikipedia.org/wiki/Master_boot_record
          'o'
        else
          raise "Unsupported partition table type: #{type}"
        end
      end

      def partition_size_code(size)
        size.nil? ? nil : "+#{size}"
      end

      def partition_type_code(type)
        case type
        when :bios
          4
        when :efi
          1
        when :linux
          20
        else
          raise "Unsupported partition type: #{type}"
        end
      end
    end
  end
end
