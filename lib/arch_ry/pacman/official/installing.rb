# frozen_string_literal: true

module ArchRy
  module Pacman
    module Official
      module Installing
        class IgnoredList
          include Enumerable

          attr_reader :root_path, :config_key

          def initialize(root_path:, config_key:)
            @root_path = root_path
            @config_key = config_key
          end

          def each(&block)
            values.each(&block)
          end

          def <<(name)
            before = values
            to_be_ignored = values
            to_be_ignored << name unless to_be_ignored.include?(name)

            TTY::Command.default.run :sed, '--in-place',
                                     "/^#\s*#{config_key}\s*=/s/^#\s*//", config_path
            TTY::Command.default.run :sed, '--in-place',
                                     "/^\s*#{config_key}\s*=/s/=.*/= #{to_be_ignored.join(' ')}/", config_path
          end

          private

          def values
            grep = TTY::Command.quiet.run! :grep, "^\s*#{config_key}\s*=", config_path
            return [] unless grep.success?

            grep.out.strip.gsub(/#{config_key}\s*=/, '').strip.split
          end

          def config_path
            "#{root_path}#{Pacman::CONFIG_FILEPATH}"
          end
        end

        module ClassMethods
          def exists?(name:)
            TTY::Command.quiet.run!(:pacman, '-S', exists_command_option, name).success?
          end
        end

        def self.included(mod)
          mod.extend(ClassMethods)
        end

        def install!(root_path = nil)
          perform_install!(root_path)
          run_post_install!(root_path) if post_install
          disable_auto_update!(root_path) unless auto_update
        end

        private

        def install_latest!(root_path)
          if root_path
            pacstrap!(root_path)
          else
            TTY::Command.default.run :pacman, '--noconfirm', '--sync', name
          end
        end

        def install_from_archive!(root_path)
          url = "https://archive.archlinux.org/packages/#{name[0]}/#{name}/#{name}-#{version}-x86_64.pkg.tar.zst"
          if root_path
            TTY::Command.default.run :pacstrap, '-U', root_path, url
          else
            TTY::Command.default.run :pacman, '--noconfirm', '-U', url
          end
        end

        def disable_auto_update!(root_path)
          IgnoredList.new(root_path: root_path, config_key: self.class.config_ignore_key) << name
        end

        def run_post_install!(root_path)
          Array(post_install).each do |command|
            if root_path
              TTY::Command.default.run :'arch-chroot', root_path, :bash, '-c', command
            else
              TTY::Command.default.run :bash, '-c', command
            end
          end
        end

        def pacstrap!(root_path)
          if respond_to?(:force) && force
            in_stream = StringIO.new
            in_stream.puts 'y'
            in_stream.puts 'y'
            in_stream.rewind
            TTY::Command.default.run :pacstrap, '-i', root_path, name, in: in_stream
          else
            TTY::Command.default.run :pacstrap, root_path, name
          end
        end
      end
    end
  end
end
