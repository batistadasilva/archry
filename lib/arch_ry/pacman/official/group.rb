# frozen_string_literal: true

module ArchRy
  module Pacman
    module Official
      # See https://archlinux.org/groups/
      class Group
        include Official::Installing

        class << self
          def config_ignore_key
            'IgnoreGroup'
          end

          def exists_command_option
            '-g'
          end
        end

        attr_reader :name, :auto_update, :post_install

        def initialize(name:, auto_update: true, post_install: nil)
          @name = name
          @auto_update = auto_update
          @post_install = post_install
        end

        private

        def perform_install!(root_path)
          install_latest!(root_path)
        end

        def ignore_config_key
          'IgnoreGroup'
        end
      end
    end
  end
end
