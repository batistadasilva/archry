# frozen_string_literal: true

require 'arch_ry/access_control/password'
require 'arch_ry/access_control/user'
require 'arch_ry/access_control/superuser'

module ArchRy
  # General access control includes authentication, authorization, and audit. A more narrow definition of access control
  # would cover only access approval, whereby the system makes a decision to grant or reject an access request from an
  # already authenticated subject, based on what the subject is authorized to access.
  #
  # See https://en.wikipedia.org/wiki/Access_control#Computer_security
  module AccessControl
  end
end
