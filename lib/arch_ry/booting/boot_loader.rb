# frozen_string_literal: true

require 'arch_ry/booting/boot_loader/bios'
require 'arch_ry/booting/boot_loader/config'
require 'arch_ry/booting/boot_loader/uefi'

module ArchRy
  module Booting
    # A boot loader is a piece of software started by the firmware (BIOS or UEFI). It is responsible for loading the
    # kernel with the wanted kernel parameters, and initial RAM disk based on configuration files. In the case of UEFI,
    # the kernel itself can be directly launched by the UEFI using the EFI boot stub. A separate boot loader or boot
    # manager can still be used for the purpose of editing kernel parameters before booting.
    #
    # See https://wiki.archlinux.org/title/Arch_boot_process#Boot_loader
    module BootLoader
    end
  end
end
