# frozen_string_literal: true

module ArchRy
  module Networking
    # A network host is a computer or other device connected to a computer network. A host may work as a server offering
    # information resources, services, and applications to users or other hosts on the network. Hosts are assigned at
    # least one network address.
    #
    # See https://en.wikipedia.org/wiki/Host_(network)
    class Host
      attr_reader :name, :address, :domain

      def initialize(name:, address:, domain: nil)
        @name = name
        @address = address
        @domain = domain
      end

      def ==(other)
        return false unless other.is_a?(Host)

        other.name == name && other.address == address && other.domain == domain
      end
    end
  end
end
