# frozen_string_literal: true

require 'singleton'

module ArchRy
  module Networking
    # A hostname is a unique name created to identify a machine on a network
    #
    # See https://wiki.archlinux.org/title/Network_configuration#Set_the_hostname
    class Hostname
      include Singleton

      FILEPATH = '/etc/hostname'

      def update!(hostname)
        TTY::Command.default.run :echo, hostname, out: FILEPATH
      end
    end
  end
end
