# frozen_string_literal: true

require 'singleton'

module ArchRy
  module Networking
    # The computer file hosts is an operating system file that maps hostnames to IP addresses.
    #
    # See https://en.wikipedia.org/wiki/Hosts_(file)
    class Hosts
      include Singleton
      include Enumerable

      FILEPATH = '/etc/hosts'

      def each(&block)
        read_from_file.each(&block)
      end

      def <<(host)
        TTY::Command.default.run(:echo, to_file_line(host), out: [FILEPATH, 'a'])
      end

      private

      def read_from_file
        TTY::Command.quiet.run(:cat, FILEPATH).reject { |line| line.start_with?('#') }.map do |line|
          entry = line.split
          address = entry.first
          hostname = entry.last
          domain = entry.length < 3 ? nil : entry[1]
          Host.new(name: hostname, address: address, domain: domain)
        end
      end

      def to_file_line(host)
        if host.domain
          "#{host.address} #{host.name}.#{host.domain} #{host.name}"
        else
          "#{host.address} #{host.name}"
        end
      end
    end
  end
end
