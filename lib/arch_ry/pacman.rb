# frozen_string_literal: true

require 'arch_ry/pacman/aur'
require 'arch_ry/pacman/official'
require 'arch_ry/pacman/own_build'

module ArchRy
  # The pacman package manager is one of the major distinguishing features of Arch Linux. It combines a simple binary
  # package format with an easy-to-use build system. The goal of pacman is to make it possible to easily manage
  # packages, whether they are from the official repositories or the user's own builds.
  #
  # See https://wiki.archlinux.org/title/Pacman
  module Pacman
    CONFIG_FILEPATH = '/etc/pacman.conf'
  end
end
