# frozen_string_literal: true

module TTY
  module Extensions
    module Prompt
      def default
        @default ||= DefaultPrompt.new
      end
    end

    class DefaultPrompt < ::TTY::Prompt
      def ask_and_be_sure(question, **options)
        answer = ask(question, **options)
        yes?('Are you sure?') ? answer : ask_and_be_sure(question, **options)
      end

      def ask_and_confirm_password(question, confirmation = 'Please confirm', **options)
        password = mask(question, **options)
        confirm = mask(confirmation, **options)
        return password if password == confirm

        puts
        puts 'Try again...'
        ask_and_confirm_password(question, confirmation, **options)
      end

      def select_and_be_sure(question, *args, &block)
        selected = select(question, *args, &block)
        yes?('Are you sure?') ? selected : select_and_be_sure(question, *args, &block)
      end
    end
  end
end

TTY::Prompt.extend(TTY::Extensions::Prompt)
