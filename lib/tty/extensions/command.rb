# frozen_string_literal: true

module TTY
  module Extensions
    module Command
      module ClassMethods
        @root_path = nil

        def default
          @default ||= TTY::Command.new
        end

        def quiet
          @quiet ||= TTY::Command.new(printer: :null)
        end

        def arch_chroot!(path)
          @root_path = path
        end

        def root_path
          @root_path
        end
      end

      module ChildProcess
        def convert(spawn_key, spawn_value)
          key, value = super
          if key == :out
            value = "#{root_path}#{value}" if value.is_a?(String) && !value.start_with?('/dev/')
            if value.is_a?(Array) && value[0].is_a?(String) && !value[0].start_with?('/dev/')
              value[0] = "#{root_path}#{value[0]}"
            end
          end
          [key, value]
        end

        private

        def root_path
          TTY::Command.root_path
        end
      end

      def run(*args)
        root_path ? super(*chroot_args(*args)) : super(*args)
      end

      def run!(*args)
        root_path ? super(*chroot_args(*args)) : super(*args)
      end

      private

      def chroot_args(*args)
        if args.last.respond_to?(:to_hash) && args.last[:user]
          user = args.last.delete(:user)
          return [:'arch-chroot', '-u', user, root_path] + args
        end

        [:'arch-chroot', root_path] + args
      end

      def root_path
        self.class.root_path
      end
    end
  end
end

TTY::Command.prepend(TTY::Extensions::Command)
TTY::Command.extend(TTY::Extensions::Command::ClassMethods)
TTY::Command::ChildProcess.singleton_class.prepend(TTY::Extensions::Command::ChildProcess)
