# frozen_string_literal: true

require 'tty-command'
require 'tty-markdown'
require 'tty-prompt'
require 'tty/extensions'
require 'arch_ry/access_control'
require 'arch_ry/booting'
require 'arch_ry/filesystem'
require 'arch_ry/locale'
require 'arch_ry/networking'
require 'arch_ry/pacman'
require 'arch_ry/service'
require 'arch_ry/time'

module ArchRy
  VERSION = '0.1.1'
end
